package ru.ovechkin.tm.entitiesForTest;

import ru.ovechkin.tm.constant.TestConst;
import ru.ovechkin.tm.dto.TaskDTO;

import java.util.ArrayList;
import java.util.List;

public class TasksForTest {

    private TaskDTO createTask(
            final String userId,
            final String id,
            final String name,
            final String description
    ) {
        final TaskDTO taskDTO = new TaskDTO();
        taskDTO.setUserId(userId);
        taskDTO.setId(id);
        taskDTO.setName(name);
        taskDTO.setDescription(description);
        return taskDTO;
    }

    final TaskDTO taskDTO1 = createTask(
            TestConst.USER_ID_USER, "testId1", "taskName1", "taskDescription");

    final TaskDTO taskDTO2 = createTask(
            TestConst.USER_ID_USER, "testId2", "taskName2", "taskDescription2");

    final TaskDTO taskDTO3 = createTask(
            TestConst.USER_ID_ADMIN, "testId11", "taskForAdmin1", "taskDescription1");

    public List<TaskDTO> getListOfTestTasks() {
        final List<TaskDTO> taskDTOList = new ArrayList<>();
        taskDTOList.add(taskDTO1);
        taskDTOList.add(taskDTO2);
        taskDTOList.add(taskDTO3);
        return taskDTOList;
    }

    public TaskDTO getTask1() {
        return taskDTO1;
    }

    public TaskDTO getTask2() {
        return taskDTO2;
    }

    public TaskDTO getTask3() {
        return taskDTO3;
    }

}