package ru.ovechkin.tm.entitiesForTest;

import ru.ovechkin.tm.constant.TestConst;
import ru.ovechkin.tm.dto.ProjectDTO;

import java.util.ArrayList;
import java.util.List;

public class ProjectsForTest {

    private ProjectDTO createProject(
            final String userId,
            final String id,
            final String name,
            final String description
    ) {
        final ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setUserId(userId);
        projectDTO.setId(id);
        projectDTO.setName(name);
        projectDTO.setDescription(description);
        return projectDTO;
    }

    final ProjectDTO projectDTO1 = createProject(
            TestConst.USER_ID_USER, "testId1", "projectName1", "projectDescription");

    final ProjectDTO projectDTO2 = createProject(
            TestConst.USER_ID_USER, "testId2", "projectName2", "projectDescription2");

    final ProjectDTO projectDTO3 = createProject(
            TestConst.USER_ID_ADMIN, "testId11", "projectForAdmin1", "projectDescription1");

    public List<ProjectDTO> getListOfTestProjects() {
        final List<ProjectDTO> testProjectDTOList = new ArrayList<>();
        testProjectDTOList.add(projectDTO1);
        testProjectDTOList.add(projectDTO2);
        testProjectDTOList.add(projectDTO3);
        return testProjectDTOList;
    }

    public ProjectDTO getProject1() {
        return projectDTO1;
    }

    public ProjectDTO getProject2() {
        return projectDTO2;
    }

    public ProjectDTO getProject3() {
        return projectDTO3;
    }

}