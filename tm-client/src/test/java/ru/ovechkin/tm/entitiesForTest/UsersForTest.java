package ru.ovechkin.tm.entitiesForTest;

import ru.ovechkin.tm.endpoint.User;
import ru.ovechkin.tm.endpoint.Role;
import ru.ovechkin.tm.util.HashUtil;

import java.util.ArrayList;
import java.util.List;

public class UsersForTest {

    private User createUser(
            final String login,
            final String password,
            final String email,
            final String firstName,
            final String middleName,
            final String lastName
    ) {
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        user.setEmail(email);
        user.setFirstName(firstName);
        user.setMiddleName(middleName);
        user.setLastName(lastName);
        return user;
    }

    final User userUserDTO = createUser(
            "testUser",
            "testUser",
            "user@test",
            "firstUser",
            "middleUser",
            "lastUser"
    );

    final User userDTOAdmin = createUser(
            "testAdmin",
            "testAdmin",
            "admin@test",
            "firstAdmin",
            "secondAdmin",
            "lastAdmin"
    );

    {
        userDTOAdmin.setRole(Role.ADMIN);
    }

    public List<User> getListOfUsers() {
        final List<User> userList = new ArrayList<>();
        userList.add(userUserDTO);
        userList.add(userDTOAdmin);
        return userList;
    }

    public User getUserUser() {
        return userUserDTO;
    }

    public User getUserAdmin() {
        return userDTOAdmin;
    }

}