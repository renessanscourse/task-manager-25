package ru.ovechkin.tm.entitiesForTest;

import ru.ovechkin.tm.endpoint.Session;
import ru.ovechkin.tm.endpoint.User;
import ru.ovechkin.tm.util.SignatureUtil;

public class SessionsForTest {

    private final UsersForTest usersForTest = new UsersForTest();

    private final User testUser = usersForTest.getUserUser();

    private Session createSession(
            final User user
    ) {
        final Session session = new Session();
        session.setUserId(user.getId());
        session.setTimestamp(System.currentTimeMillis());
        final String sessionString = session.toString();
        session.setSignature(SignatureUtil.sign(sessionString, "testSalt", 1337));
        return session;
    }

    private final Session sessionOfTestUser = createSession(testUser);

    public Session getSessionOfTestUser() {
        return sessionOfTestUser;
    }
}