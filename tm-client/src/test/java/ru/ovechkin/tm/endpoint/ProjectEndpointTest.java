package ru.ovechkin.tm.endpoint;

import com.sun.xml.internal.ws.fault.ServerSOAPFaultException;
import org.junit.Assert;
import org.junit.Test;
import ru.ovechkin.tm.api.locator.IEndpointLocator;
import ru.ovechkin.tm.locator.EndpointLocator;

public class ProjectEndpointTest {

    private final IEndpointLocator endpointLocator = new EndpointLocator();

    private final AuthEndpoint authEndpoint = endpointLocator.getAuthEndpoint();

    private final ProjectEndpoint projectEndpoint = endpointLocator.getProjectEndpoint();

    final Session openedSession = authEndpoint.login("user", "user");

    @Test
    public void testCreatePositive() {
        projectEndpoint.createProjectWithName(openedSession, "testProject");
        Assert.assertNotNull(projectEndpoint.findProjectByIndex(openedSession, 1));
        projectEndpoint.removeProjectByIndex(openedSession, 1);
        Assert.assertThrows(
                ServerSOAPFaultException.class,
                () -> projectEndpoint.findProjectByIndex(openedSession, 1));
        authEndpoint.logout(openedSession);
    }

    @Test
    public void testCreateWithDescription() {
        projectEndpoint.createProjectWithNameAndDescription(openedSession, "testProject", "description");
        Assert.assertNotNull(projectEndpoint.findProjectByIndex(openedSession, 1));
        projectEndpoint.removeProjectByIndex(openedSession, 1);
        Assert.assertThrows(
                ServerSOAPFaultException.class,
                () -> projectEndpoint.findProjectByIndex(openedSession, 1));
        authEndpoint.logout(openedSession);
    }

    @Test
    public void testShowListOfUserProjects() {
        projectEndpoint.createProjectWithName(openedSession, "testProject");
        projectEndpoint.createProjectWithName(openedSession, "test2");
        Assert.assertEquals(2, projectEndpoint.findUserProjects(openedSession).size());
        projectEndpoint.removeAllProjects(openedSession);
        authEndpoint.logout(openedSession);
    }

    @Test
    public void testRemoveAllProjects() {
        projectEndpoint.createProjectWithName(openedSession, "testProject");
        projectEndpoint.createProjectWithName(openedSession, "test2");
        Assert.assertEquals(2, projectEndpoint.findUserProjects(openedSession).size());
        projectEndpoint.removeAllProjects(openedSession);
        Assert.assertEquals(0, projectEndpoint.findUserProjects(openedSession).size());
        authEndpoint.logout(openedSession);
    }

    @Test
    public void testFindProjectByIndex() {
        projectEndpoint.createProjectWithNameAndDescription(openedSession, "testProject", "description");
        Assert.assertNotNull(projectEndpoint.findProjectByIndex(openedSession, 1));
        projectEndpoint.removeProjectByIndex(openedSession, 1);
        Assert.assertThrows(
                ServerSOAPFaultException.class,
                () -> projectEndpoint.findProjectByIndex(openedSession, 1));
        authEndpoint.logout(openedSession);
    }

    @Test
    public void testFindProjectByName() {
        projectEndpoint.createProjectWithNameAndDescription(openedSession, "testProject", "description");
        Assert.assertNotNull(projectEndpoint.findProjectByName(openedSession, "testProject"));
        projectEndpoint.removeProjectByName(openedSession, "testProject");
        Assert.assertThrows(
                ServerSOAPFaultException.class,
                () -> projectEndpoint.removeProjectByName(openedSession, "testProject"));
        authEndpoint.logout(openedSession);
    }

    @Test
    public void testUpdateProjectByIndex() {
        projectEndpoint.createProjectWithNameAndDescription(openedSession, "testProject", "description");
        projectEndpoint.updateProjectByIndex(openedSession, 1, "newName", "newDescription");
        Assert.assertNotNull(projectEndpoint.findProjectByName(openedSession, "newName"));
        projectEndpoint.removeProjectByName(openedSession, "newName");
        authEndpoint.logout(openedSession);
    }

    @Test
    public void testRemoveProjectByIndex() {
        projectEndpoint.createProjectWithNameAndDescription(openedSession, "testProject", "description");
        Assert.assertNotNull(projectEndpoint.findProjectByIndex(openedSession, 1));
        projectEndpoint.removeProjectByIndex(openedSession, 1);
        Assert.assertThrows(
                ServerSOAPFaultException.class,
                () -> projectEndpoint.findProjectByIndex(openedSession, 1));
        authEndpoint.logout(openedSession);
    }

    @Test
    public void testRemoveProjectByName() {
        projectEndpoint.createProjectWithNameAndDescription(openedSession, "testProject", "description");
        Assert.assertNotNull(projectEndpoint.findProjectByName(openedSession, "testProject"));
        projectEndpoint.removeProjectByName(openedSession, "testProject");
        Assert.assertThrows(
                ServerSOAPFaultException.class,
                () -> projectEndpoint.removeProjectByName(openedSession, "testProject"));
        authEndpoint.logout(openedSession);
    }

}