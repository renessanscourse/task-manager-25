package ru.ovechkin.tm.endpoint;

import com.sun.xml.internal.ws.fault.ServerSOAPFaultException;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import ru.ovechkin.tm.api.locator.IEndpointLocator;
import ru.ovechkin.tm.api.locator.IServiceLocator;
import ru.ovechkin.tm.entitiesForTest.UsersForTest;
import ru.ovechkin.tm.exeption.other.AccessForbiddenException;
import ru.ovechkin.tm.locator.EndpointLocator;
import ru.ovechkin.tm.locator.ServiceLocator;

public class SessionDTOEndpointTest {

    private final IServiceLocator serviceLocator = new ServiceLocator();

    private final IEndpointLocator endpointLocator = new EndpointLocator();

    private final SessionEndpoint sessionEndpoint = endpointLocator.getSessionEndpoint();


    private final UsersForTest usersForTest = new UsersForTest();

    private final User user = usersForTest.getUserUser();


    private final Session openedSession = sessionEndpoint.openSession("user", "user");

    @Test
    public void testOpenSession() {
        Assert.assertTrue(sessionEndpoint.isValid(openedSession));
    }

    @Test
    public void testCloseSession() {
        Assert.assertTrue(sessionEndpoint.isValid(openedSession));
        sessionEndpoint.closeSession(openedSession);
        Assert.assertFalse(sessionEndpoint.isValid(openedSession));
    }

    @Test
    public void testSessionsOfUser() {
        sessionEndpoint.openSession("user", "user");
        Assert.assertEquals(2, sessionEndpoint.sessionsOfUser(openedSession).size());
    }

    @Test
    public void testIsValid() {
        Assert.assertTrue(sessionEndpoint.isValid(openedSession));
    }

    @Test
    public void testSignOutByLogin() {
        Assert.assertTrue(sessionEndpoint.isValid(openedSession));
        sessionEndpoint.signOutByLogin("user");
        Assert.assertThrows(
                ServerSOAPFaultException.class,
                () -> sessionEndpoint.isValid(openedSession));
    }

    @Test
    public void testSignOutByUserId() {
        Assert.assertTrue(sessionEndpoint.isValid(openedSession));
        sessionEndpoint.signOutByUserId("");//тут нужно указать айдишник серверного юзера
        Assert.assertThrows(
                ServerSOAPFaultException.class,
                () -> sessionEndpoint.isValid(openedSession));
    }

    @Test
    @Ignore
    public void testGetUser() {
        Assert.assertEquals(user, sessionEndpoint.getUser(openedSession));//тут нуэно передать серверного юзера
    }

    @Test
    public void testCloseAll() {
        sessionEndpoint.openSession("user", "user");
        sessionEndpoint.closeAll(openedSession);
        Assert.assertEquals(0, serviceLocator.getSessionService().findAll().size());
    }

}
