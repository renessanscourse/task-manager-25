package ru.ovechkin.tm.endpoint;

import org.junit.Assert;
import org.junit.Test;
import ru.ovechkin.tm.api.locator.IEndpointLocator;
import ru.ovechkin.tm.locator.EndpointLocator;

public class TaskDTOEndpointTest {

    private final IEndpointLocator endpointLocator = new EndpointLocator();

    private final AuthEndpoint authEndpoint = endpointLocator.getAuthEndpoint();

    private final TaskEndpoint taskEndpoint = endpointLocator.getTaskEndpoint();

    final Session openedSession = authEndpoint.login("user", "user");

    @Test
    public void testCreatePositive() {
        taskEndpoint.createTaskWithName(openedSession, "testTask");
        Assert.assertNotNull(taskEndpoint.findTaskByIndex(openedSession, 1));
        taskEndpoint.removeTaskByIndex(openedSession, 1);
        Assert.assertNull(taskEndpoint.findTaskByIndex(openedSession, 1));
        authEndpoint.logout(openedSession);
    }

    @Test
    public void testCreateWithDescription() {
        taskEndpoint.createTaskWithNameAndDescription(openedSession, "testTask", "description");
        Assert.assertNotNull(taskEndpoint.findTaskByIndex(openedSession, 1));
        taskEndpoint.removeTaskByIndex(openedSession, 1);
        Assert.assertNull(taskEndpoint.findTaskByIndex(openedSession, 1));
        authEndpoint.logout(openedSession);
    }

    @Test
    public void testShowListOfUserTasks() {
        taskEndpoint.createTaskWithName(openedSession, "testTask");
        taskEndpoint.createTaskWithName(openedSession, "test2");
        Assert.assertEquals(2, taskEndpoint.findUserTasks(openedSession).size());
        taskEndpoint.removeAllTasks(openedSession);
        authEndpoint.logout(openedSession);
    }

    @Test
    public void testRemoveAllTasks() {
        taskEndpoint.createTaskWithName(openedSession, "testTask");
        taskEndpoint.createTaskWithName(openedSession, "test2");
        Assert.assertEquals(2, taskEndpoint.findUserTasks(openedSession).size());
        taskEndpoint.removeAllTasks(openedSession);
        Assert.assertEquals(0, taskEndpoint.findUserTasks(openedSession).size());
        authEndpoint.logout(openedSession);
    }

    @Test
    public void testFindTaskByIndex() {
        taskEndpoint.createTaskWithNameAndDescription(openedSession, "testTask", "description");
        Assert.assertNotNull(taskEndpoint.findTaskByIndex(openedSession, 1));
        taskEndpoint.removeTaskByIndex(openedSession, 1);
        Assert.assertNull(taskEndpoint.findTaskByIndex(openedSession, 1));
        authEndpoint.logout(openedSession);
    }

    @Test
    public void testFindTaskByName() {
        taskEndpoint.createTaskWithNameAndDescription(openedSession, "testTask", "description");
        Assert.assertNotNull(taskEndpoint.findTaskByName(openedSession, "testTask"));
        taskEndpoint.removeTaskByName(openedSession, "testTask");
        Assert.assertNull(taskEndpoint.findTaskByIndex(openedSession, 1));

        authEndpoint.logout(openedSession);
    }

    @Test
    public void testUpdateTaskByIndex() {
        taskEndpoint.createTaskWithNameAndDescription(openedSession, "testTask", "description");
        taskEndpoint.updateTaskByIndex(openedSession, 1, "newName", "newDescription");
        Assert.assertNotNull(taskEndpoint.findTaskByName(openedSession, "newName"));
        taskEndpoint.removeTaskByName(openedSession, "newName");
        authEndpoint.logout(openedSession);
    }

    @Test
    public void testRemoveTaskByIndex() {
        taskEndpoint.createTaskWithNameAndDescription(openedSession, "testTask", "description");
        Assert.assertNotNull(taskEndpoint.findTaskByIndex(openedSession, 1));
        taskEndpoint.removeTaskByIndex(openedSession, 1);
        Assert.assertNull(taskEndpoint.findTaskByIndex(openedSession, 1));
        authEndpoint.logout(openedSession);
    }

    @Test
    public void testRemoveTaskByName() {
        taskEndpoint.createTaskWithNameAndDescription(openedSession, "testTask", "description");
        Assert.assertNotNull(taskEndpoint.findTaskByName(openedSession, "testTask"));
        taskEndpoint.removeTaskByName(openedSession, "testTask");
        Assert.assertNull(taskEndpoint.findTaskByIndex(openedSession, 1));
        authEndpoint.logout(openedSession);
    }

}