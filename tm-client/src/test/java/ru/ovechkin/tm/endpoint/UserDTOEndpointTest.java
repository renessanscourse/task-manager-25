package ru.ovechkin.tm.endpoint;

import com.sun.xml.internal.ws.fault.ServerSOAPFaultException;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import ru.ovechkin.tm.api.locator.IServiceLocator;
import ru.ovechkin.tm.entitiesForTest.UsersForTest;
import ru.ovechkin.tm.endpoint.User;
import ru.ovechkin.tm.endpoint.Role;
import ru.ovechkin.tm.exeption.unknown.LoginUnknownException;
import ru.ovechkin.tm.exeption.user.AccessDeniedException;
import ru.ovechkin.tm.locator.EndpointLocator;
import ru.ovechkin.tm.locator.ServiceLocator;

public class UserDTOEndpointTest {

    private final IServiceLocator serviceLocator = new ServiceLocator();

    private final EndpointLocator endpointLocator = new EndpointLocator();

    private final SessionEndpoint sessionEndpoint = endpointLocator.getSessionEndpoint();

    private final UserEndpoint userEndpoint = endpointLocator.getUserEndpoint();


    private final Session openedSession = sessionEndpoint.openSession("user", "user");

    @Test
    public void testFindByIdPositive() {
        final Session adminSession = sessionEndpoint.openSession("admin", "admin");
        final User user = userEndpoint.createWithLoginAndPassword(openedSession, "test2", "test");
        Assert.assertEquals(user.toString(), userEndpoint.findById(adminSession, user.getId()).toString());
        userEndpoint.removeByLogin(adminSession, user.getLogin());
        Assert.assertNull(userEndpoint.findByLogin(adminSession, user.getLogin()));
    }

    @Test
    public void testFindByLoginPositive() {
        final Session adminSession = sessionEndpoint.openSession("admin", "admin");
        Assert.assertNotNull(userEndpoint.findByLogin(adminSession, "user"));
    }

    @Test
    public void testCreateWithLoginAndPasswordPositive() {
        final Session adminSession = sessionEndpoint.openSession("admin", "admin");
        final User user = userEndpoint.createWithLoginAndPassword(openedSession, "test2", "test");
        Assert.assertEquals(user.toString(), userEndpoint.findByLogin(adminSession, "test2").toString());
        userEndpoint.removeByLogin(adminSession, user.getLogin());
        Assert.assertNull(userEndpoint.findByLogin(adminSession, user.getLogin()));
    }

    @Test
    public void testCreateWithEmailPositive() {
        final Session adminSession = sessionEndpoint.openSession("admin", "admin");
        final User user = userEndpoint.createWithLoginPasswordAndEmail(
                adminSession, "test2", "test2", "test2");
        Assert.assertEquals(user.toString(), userEndpoint.findByLogin(adminSession, "test2").toString());
        userEndpoint.removeByLogin(adminSession, "test2");
        Assert.assertNull(userEndpoint.findByLogin(adminSession, "test2"));
    }

    @Test
    public void testCreateWithRolePositive() {
        final Session adminSession = sessionEndpoint.openSession("admin", "admin");
        final User user = userEndpoint.createWithLoginPasswordAndRole(
                adminSession, "test3", "test2", Role.ADMIN);
        Assert.assertEquals(user.toString(), userEndpoint.findByLogin(adminSession, "test3").toString());
    }

    @Test
    public void testLockUserByLogin() {
        sessionEndpoint.closeSession(openedSession);
        final Session adminSession = sessionEndpoint.openSession("admin", "admin");
        userEndpoint.lockUserByLogin(adminSession, "user");
        sessionEndpoint.closeSession(adminSession);
        Assert.assertThrows(
                ServerSOAPFaultException.class,//ServerSOAPFaultException
                () -> endpointLocator.getAuthEndpoint().login("user", "user"));
    }

    @Test
    public void testUnlockUserByLogin() {
        final Session adminSession = sessionEndpoint.openSession("admin", "admin");
        userEndpoint.lockUserByLogin(adminSession, "user");
        Assert.assertThrows(
                ServerSOAPFaultException.class,
                () -> endpointLocator.getAuthEndpoint().login("user", "user"));
        userEndpoint.unLockUserByLogin(adminSession, "user");
        serviceLocator.getAuthService().login("user", "user");
        serviceLocator.getAuthService().logout();
    }

    @Test
    public void testRemoveByIdPositive() {
        final Session adminSession = sessionEndpoint.openSession("admin", "admin");
        final User user = userEndpoint.createWithLoginAndPassword(openedSession, "test4", "test4");
        Assert.assertEquals(user.toString(), userEndpoint.findById(adminSession, user.getId()).toString());
        userEndpoint.removeById(adminSession, user.getId());
        Assert.assertThrows(
                ServerSOAPFaultException.class,
                () -> endpointLocator.getAuthEndpoint().login("test4", "test4"));
    }

    @Test
    public void testRemoveByLoginPositive() {
        final Session adminSession = sessionEndpoint.openSession("admin", "admin");
        final User user = userEndpoint.createWithLoginAndPassword(openedSession, "test5", "test5");
        Assert.assertEquals(user.toString(), userEndpoint.findByLogin(adminSession, user.getLogin()).toString());
        userEndpoint.removeById(adminSession, user.getId());
        Assert.assertThrows(
                ServerSOAPFaultException.class,
                () -> endpointLocator.getAuthEndpoint().login("test4", "test4"));
    }

}