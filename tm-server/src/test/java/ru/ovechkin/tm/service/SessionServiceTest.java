package ru.ovechkin.tm.service;

import org.junit.Assert;
import org.junit.Test;
import ru.ovechkin.tm.api.locator.IServiceLocator;
import ru.ovechkin.tm.api.service.ISessionService;
import ru.ovechkin.tm.dto.SessionDTO;
import ru.ovechkin.tm.dto.UserDTO;
import ru.ovechkin.tm.enumirated.Role;
import ru.ovechkin.tm.locator.ServiceLocator;

public class SessionServiceTest {

    final IServiceLocator serviceLocator = new ServiceLocator();

    final ISessionService sessionService = new SessionService(serviceLocator);

    @Test
    public void testCheckDataAccess() {
        Assert.assertTrue(sessionService.checkDataAccess("admin", "admin"));
    }

    @Test
    public void testValidate() {
        final SessionDTO sessionDTO = sessionService.open("admin", "admin");
        Assert.assertTrue(sessionService.isValid(sessionDTO));
    }

    @Test
    public void testValidateWithRole() {
        final SessionDTO sessionDTO = sessionService.open("admin", "admin");
        sessionService.validate(sessionDTO, Role.ADMIN);
    }

    @Test
    public void testOpen() {
        sessionService.open("admin", "admin");
    }

    @Test
    public void testSignOutByLogin() {
        final SessionDTO sessionDTO = sessionService.open("admin", "admin");
        sessionService.signOutByLogin("admin");
        Assert.assertFalse(sessionService.isValid(sessionDTO));
    }

    @Test
    public void testSignOutBuUserId() {
        final SessionDTO sessionDTO = sessionService.open("admin", "admin");
        sessionService.signOutByUserId(sessionDTO.getUserId());
        Assert.assertFalse(sessionService.isValid(sessionDTO));
    }

    @Test
    public void testGetUser() {
        final UserDTO userDTO = serviceLocator.getUserService().findByLogin("admin");
        final SessionDTO sessionDTO = sessionService.open("admin", "admin");
        Assert.assertEquals(userDTO, sessionService.getUser(sessionDTO));
    }

    @Test
    public void testGetUserId() {
        final UserDTO userDTO = serviceLocator.getUserService().findByLogin("admin");
        final SessionDTO sessionDTO = sessionService.open("admin", "admin");
        Assert.assertEquals(userDTO.getId(), sessionService.getUserId(sessionDTO));
    }

    @Test
    public void testGetListSessions() {
        final SessionDTO sessionDTO = sessionService.open("admin", "admin");
        sessionService.open("admin", "admin");
        Assert.assertEquals(2, sessionService.getListSession(sessionDTO).size());
    }

    @Test
    public void testRemove() {
        final SessionDTO sessionDTO = sessionService.open("admin", "admin");
        sessionService.close(sessionDTO);
    }
}