package ru.ovechkin.tm.service;

import org.junit.Test;
import ru.ovechkin.tm.api.locator.IServiceLocator;
import ru.ovechkin.tm.api.service.ITaskService;
import ru.ovechkin.tm.dto.ProjectDTO;
import ru.ovechkin.tm.dto.TaskDTO;
import ru.ovechkin.tm.dto.SessionDTO;
import ru.ovechkin.tm.locator.ServiceLocator;

public class TaskServiceTest {

    final IServiceLocator serviceLocator = new ServiceLocator();

    final ITaskService taskService = new TaskService(serviceLocator);

    @Test
    public void testAdd() {
        final SessionDTO sessionDTO = serviceLocator.getSessionService().open("admin", "admin");
        final TaskDTO taskDTO = new TaskDTO();
        taskDTO.setName("testTaskDTO1");
        taskDTO.setDescription("testDescription");
        final ProjectDTO projectDTO = serviceLocator.getProjectService().findProjectById(
                sessionDTO.getUserId(),
                "d38040c0-3b1f-44b8-a47e-1ddb278a18c4");
        taskService.add(sessionDTO, taskDTO, projectDTO);
        serviceLocator.getSessionService().signOutByLogin("admin");
    }

    @Test
    public void testCreateWithName() {
        final SessionDTO sessionDTO = serviceLocator.getSessionService().open("admin", "admin");
//        taskService.create(sessionDTO, "testCreateWithName");
        serviceLocator.getSessionService().signOutByLogin("admin");
    }

    @Test
    public void testCreateWithNameAndDescription() {
        final SessionDTO sessionDTO = serviceLocator.getSessionService().open("admin", "admin");
        taskService.create(sessionDTO, "testCreateWithDescription", "testDescription");
        serviceLocator.getSessionService().signOutByLogin("admin");
    }

    @Test
    public void testGetTasksDto() {
        System.out.println(taskService.findUserTasks("22bd73ee-90f3-4cde-b4ca-182397d3adfa"));
    }

    @Test
    public void testRemoveAll() {
        taskService.removeAllTasks();
    }

    @Test
    public void testFindTaskById() {
        System.out.println(taskService.findTaskById(
                "22bd73ee-90f3-4cde-b4ca-182397d3adfa",
                "36f95f88-fa67-4405-8e08-32d462ac9f27"));
    }

    @Test
    public void testFindTaskByName() {
        System.out.println(taskService.findTaskByName(
                "22bd73ee-90f3-4cde-b4ca-182397d3adfa",
                "testTaskDTO2"));
    }

    @Test
    public void testUpdateTaskById() {
        taskService.updateTaskById(
                "22bd73ee-90f3-4cde-b4ca-182397d3adfa",
                "36f95f88-fa67-4405-8e08-32d462ac9f27",
                "taskChanged",
                "changedDesc");
    }

    @Test
    public void testRemoveTaskById() {
        taskService.removeTaskById(
                "22bd73ee-90f3-4cde-b4ca-182397d3adfa",
                "36f95f88-fa67-4405-8e08-32d462ac9f27");
    }

    @Test
    public void testRemoveTaskByName() {
        taskService.removeTaskByName(
                "22bd73ee-90f3-4cde-b4ca-182397d3adfa",
                "testTaskDTO1");
    }
}