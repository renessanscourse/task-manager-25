package ru.ovechkin.tm.repository;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.ovechkin.tm.api.locator.IServiceLocator;
import ru.ovechkin.tm.api.repository.IUserRepository;
import ru.ovechkin.tm.api.service.IEntityManagerService;
import ru.ovechkin.tm.entity.User;
import ru.ovechkin.tm.enumirated.Role;
import ru.ovechkin.tm.locator.ServiceLocator;
import ru.ovechkin.tm.util.HashUtil;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.transaction.Transaction;

public class UserRepositoryForTest {

    private final IServiceLocator serviceLocator = new ServiceLocator();

    private final IEntityManagerService entityManagerService = serviceLocator.getEntityManagerService();

    {
        try {
            entityManagerService.init();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private final EntityManager entityManager = entityManagerService.getEntityManager();

    private final IUserRepository userRepository = new UserRepository(entityManager);

    @Test
    public void testAdd() {
        userRepository.add(new User("user", HashUtil.salt("user"), Role.USER));
    }

    @Test
    public void testFindUserById() {
        final User user = userRepository.findById("9957439f-e2f6-4126-9a80-9695dcd827c9");
        if (user == null) throw new RuntimeException("User is null...");
        System.out.println(user.getLogin());
    }

    @Test
    public void testFindUserByLogin() {
        final User user = userRepository.findByLogin("createFromService");
        if (user == null) throw new RuntimeException("User is null...");
        System.out.println(user.getLogin());
    }

    @Test
    public void testRemoveUser() {
        final User user = new User("forRemove", HashUtil.salt("user"), Role.USER);
        user.setId("test");
        final EntityManager entityManager = entityManagerService.getEntityManager();
        final EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        entityManager.persist(user);
        transaction.commit();
        entityManager.clear();
        userRepository.removeUser(user);
    }

    @Test
    public void testRemoveById() {
        final User user = new User("forRemove", HashUtil.salt("user"), Role.USER);
        user.setId("test");
        final EntityManager entityManager = entityManagerService.getEntityManager();
        final EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        entityManager.persist(user);
        transaction.commit();
        entityManager.clear();
        userRepository.removeById(user.getId());
    }

    @Test
    public void testRemoveByLogin() {
        final User user = new User("forRemove", HashUtil.salt("user"), Role.USER);
        user.setId("test");
        final EntityManager entityManager = entityManagerService.getEntityManager();
        final EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        entityManager.persist(user);
        transaction.commit();
        entityManager.clear();
        userRepository.removeByLogin(user.getLogin());
    }

}