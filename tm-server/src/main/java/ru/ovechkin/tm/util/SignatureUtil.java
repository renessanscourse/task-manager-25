package ru.ovechkin.tm.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface SignatureUtil {

    @Nullable
    static String sign(Object value, String salt, Integer cycle) {
        try {
            final ObjectMapper objectMapper = new ObjectMapper();
            final String json = objectMapper.writeValueAsString(value);
            return sign(json, salt, cycle);
        } catch (final JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }
    }

    static String sign(String value, String salt, Integer cycle) {
        if (value == null || salt == null) return null;
        String result = value;
        for (int i = 0; i < cycle; i++) {
            result = HashUtil.md5(salt + result + salt);
        }
        return result;
    }

}
