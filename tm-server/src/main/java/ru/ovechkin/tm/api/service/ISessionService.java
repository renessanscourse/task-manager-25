package ru.ovechkin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.dto.SessionDTO;
import ru.ovechkin.tm.dto.UserDTO;
import ru.ovechkin.tm.entity.Session;
import ru.ovechkin.tm.enumirated.Role;
import ru.ovechkin.tm.exeption.other.AccessForbiddenException;

import java.util.List;

public interface ISessionService {

    boolean checkDataAccess(@Nullable String login, @Nullable String password);

    boolean isValid(@Nullable SessionDTO sessionDTO);

    @NotNull SessionDTO open(@Nullable String login, @Nullable String password);

    void validate(@Nullable SessionDTO sessionDTO);

    void validate(@Nullable SessionDTO sessionDTO, @Nullable Role role);

    @NotNull SessionDTO sign(@Nullable SessionDTO sessionDTO);

    void signOutByLogin(@Nullable String login);

    void signOutByUserId(@Nullable String userId);

    @NotNull UserDTO getUser(@Nullable SessionDTO sessionDTO) throws AccessForbiddenException;

    @NotNull String getUserId(@Nullable SessionDTO sessionDTO) throws AccessForbiddenException;

    @NotNull List<SessionDTO> getListSession(@Nullable SessionDTO sessionDTO) throws AccessForbiddenException;

    void close(@Nullable SessionDTO sessionDTO) throws AccessForbiddenException;

    void closeAll(@Nullable SessionDTO sessionDTO) throws AccessForbiddenException;

}