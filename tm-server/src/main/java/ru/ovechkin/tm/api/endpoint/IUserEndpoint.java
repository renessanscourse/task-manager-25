package ru.ovechkin.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.dto.SessionDTO;
import ru.ovechkin.tm.dto.UserDTO;
import ru.ovechkin.tm.enumirated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;

public interface IUserEndpoint {

    @Nullable
    @WebMethod
    UserDTO findById(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "id", partName = "id") final String id
    );

    @Nullable
    @WebMethod
    UserDTO findByLogin(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "login", partName = "login") final String login
    );

    @Nullable
    @WebMethod
    UserDTO removeById(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "id", partName = "id") final String id
    );

    @Nullable
    @WebMethod
    UserDTO removeByLogin(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "login", partName = "login") final String login
    );

    @Nullable
    @WebMethod
    UserDTO createWithLoginAndPassword(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "login", partName = "login") final String login,
            @Nullable @WebParam(name = "password", partName = "password") final String password
    );

    @Nullable
    @WebMethod
    UserDTO createWithLoginPasswordAndRole(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "login", partName = "login") final String login,
            @Nullable @WebParam(name = "password", partName = "password") final String password,
            @Nullable @WebParam(name = "role", partName = "role")final Role role
    );

    @Nullable
    @WebMethod
    UserDTO lockUserByLogin(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "login", partName = "login") final String login
    );

    @Nullable
    @WebMethod
    UserDTO unLockUserByLogin(
            @Nullable @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO,
            @Nullable @WebParam(name = "login", partName = "login") final String login
    );

    @WebMethod
    void updateProfileLogin(
            @Nullable @WebParam(name = "session", partName = "session") SessionDTO sessionDTO,
            @Nullable @WebParam(name = "newLogin", partName = "newLogin") String newLogin
            );

    @WebMethod
    void updatePassword(
            @Nullable @WebParam(name = "session", partName = "session") SessionDTO sessionDTO,
            @Nullable @WebParam(name = "currentPassword", partName = "currentPassword") String currentPassword,
            @Nullable @WebParam(name = "newPassword", partName = "newEmail") String newPassword
    );
}