package ru.ovechkin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.dto.UserDTO;
import ru.ovechkin.tm.entity.User;
import ru.ovechkin.tm.enumirated.Role;

import java.util.List;

public interface IUserService {

    @Nullable
    UserDTO create(String login, String password);

    @Nullable
    UserDTO create(String login, String password, Role role);

    @NotNull
    UserDTO findById(String id);

    @Nullable
    UserDTO findByLogin(String login);

    @NotNull
    UserDTO removeUser(UserDTO userDTO);

    @NotNull
    UserDTO removeById(String adminId, String id);

    @NotNull
    UserDTO removeByLogin(String userId, String login);

    @Nullable
    UserDTO lockUserByLogin(String userId, String login);

    @Nullable
    UserDTO unLockUserByLogin(String userId, String login);

    @NotNull List<UserDTO> getAllUsersDTO();

    @NotNull List<UserDTO> loadUsers(@Nullable List<UserDTO> usersDTO);

    void removeAllUsers();

    @NotNull UserDTO updateProfileLogin(
            @Nullable String userId,
            @Nullable String newLogin
    );

    void updatePassword(
            @Nullable String userId,
            @Nullable String currentPassword,
            @Nullable String newPassword
    );
}