!#/usr/bin/env bash

echo "reboot task-manager-server"

if [ ! -f tm-server.pid ]; then
	echo "tm-server pid not found! server is not running!"
	exit 1;
fi

echo 'process with pid '$(cat tm-server.pid)' was killed';
kill -9 $(cat tm-server.pid)
rm tm-server.pid

mkdir -p ../bash/logs
rm -f ../bash/logs/tm-server.log
nohup java -jar ~/task-manager-25/tm-server/target/tm-server-jar-with-dependencies.jar > ../bash/logs/tm-server.log 2>&1 &
echo $! > tm-server.pid
echo "task-manager is running with pid "$!
