# PROJECT INFO

TASK MANAGER

# DEVELOPER INFO

**NAME**: OVECHKIN ROMAN

**E-MAIL**: ovechkin@roman.ru

# SOFTWARE

- JDK 1.8

- Windows 10

# PROGRAM BUILD

```bash
mvn clean install
```

# PROGRAM RUN

```bash
java -jar ./taskDTO-manager.jar
```

# SCREENSHOTS
СкринШоты работы в линуксе - https://yadi.sk/d/DJb9a4i2LyE8Qg?w=1
